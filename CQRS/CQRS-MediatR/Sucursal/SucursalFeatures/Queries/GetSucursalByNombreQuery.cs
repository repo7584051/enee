﻿using CQRS_MediatR.Context;
using CQRS_MediatR.Entity;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace CQRS_MediatR.Features.TransactionFeatures.Queries
{
    public class GetSucursalByNombreQuery : IRequest<Sucursal>
    {
        public string Nombre { get; set; }
        public class GetSucursalByNombreQueryHandler : IRequestHandler<GetSucursalByNombreQuery, Sucursal>
        {
            private readonly ISucursalDbContext _context;
            public GetSucursalByNombreQueryHandler(ISucursalDbContext context)
            {
                _context = context;
            }
            public async Task<Sucursal> Handle(GetSucursalByNombreQuery query, CancellationToken cancellationToken)
            {
                var transcation = _context.Sucursal.Where(a => a.NombreAdministrador == query.Nombre || a.NombreSucursal == query.Nombre && !a.IsSoftDeleted).FirstOrDefault();
                if (transcation == null) return null;
                return transcation;

            }
        }
    }
}
