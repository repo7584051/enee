﻿using CQRS_MediatR.Context;
using FluentValidation;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace CQRS_MediatR.Features.TransactionFeatures.Commands
{
    public class UpdateSucursalCommand : IRequest<string>
    {
        public Guid Id { get; set; }
        public string NombreSucursal { get; set; }
        public string NombreAdministrador { get; set; }
        public string Telefono { get; set; }
        public string Direccion { get; set; }
        public string Fax { get; set; }
        public int NumeroPedidoMes { get; set; }
        
        public class UpdateSucursalCommandHandler : IRequestHandler<UpdateSucursalCommand, string>
        {
            private readonly ISucursalDbContext _context;
            public UpdateSucursalCommandHandler(ISucursalDbContext context)
            {
                _context = context;
            }
            
            public async Task<string> Handle(UpdateSucursalCommand command, CancellationToken cancellationToken)
            {
                var validator = new SucursalCommandValidator(_context);
                var validatorReult = await validator.ValidateAsync(command);
                if (!validatorReult.IsValid) { return validatorReult.Errors.FirstOrDefault().ErrorMessage; }
                var suc = _context.Sucursal.Where(a => a.Id == command.Id).FirstOrDefault();
                if (suc == null)
                {
                    return default;
                }
                else
                {
                    suc.NombreSucursal = command.NombreSucursal;
                    suc.NombreAdministrador = command.NombreAdministrador;
                    suc.Telefono = command.Telefono;
                    suc.Direccion = command.Direccion;
                    suc.Fax = command.Fax;
                    suc.NumeroPedidoMes = command.NumeroPedidoMes;
                    suc.Modifico();
                    await _context.SaveChangesAsync();
                    return suc.Id.ToString();
                }
            }
            
        }
    }
}
