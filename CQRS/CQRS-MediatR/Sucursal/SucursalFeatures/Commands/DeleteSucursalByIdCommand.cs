﻿using CQRS_MediatR.Context;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace CQRS_MediatR.Features.TransactionFeatures.Commands
{
    public class DeleteSucursalByIdCommand : IRequest<Guid>
    {
        public Guid Id { get; set; }
        public class DeleteSucursalByIdCommandHandler : IRequestHandler<DeleteSucursalByIdCommand, Guid>
        {
            private readonly ISucursalDbContext _context;
            public DeleteSucursalByIdCommandHandler(ISucursalDbContext context)
            {
                _context = context;
            }
            public async Task<Guid> Handle(DeleteSucursalByIdCommand command, CancellationToken cancellationToken)
            {
                var sucursal = _context.Sucursal.Where(a => a.Id == command.Id).FirstOrDefault();
                if (sucursal == null) return default;
                sucursal.Delete();
                sucursal.Modifico();
                await _context.SaveChangesAsync();
                return sucursal.Id;
            }
        }
    }
}