﻿using CQRS_MediatR.Entity;
using Microsoft.EntityFrameworkCore;
using System.Threading.Tasks;

namespace CQRS_MediatR.Context
{
    public interface ISucursalDbContext
    {
        DbSet<Sucursal> Sucursal { get; set; }

        Task<int> SaveChangesAsync();
       
    }
}