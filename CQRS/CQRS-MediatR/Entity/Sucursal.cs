﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CQRS_MediatR.Entity
{
    public class Sucursal
    {
        public Guid Id { get; set; } = Guid.NewGuid();
        public string NombreSucursal { get; set; }
        public string NombreAdministrador { get; set; }
        public string Telefono { get; set; }
        public string Direccion { get; set; }
        public string Fax { get; set; }
        public int NumeroPedidoMes { get; set; }
        public DateTime FechaCreacion { get; set; } = DateTime.Now;
        public DateTime? FechaModificacion { get; set; }
        public bool IsSoftDeleted { get; set; } = false;
        #region publicmethods
        public void Delete()
        {
            IsSoftDeleted = true;
        }
        public void Modifico()
        {
            var fecha = DateTime.Now;
            FechaModificacion = fecha;
        }
        #endregion
    }
}
