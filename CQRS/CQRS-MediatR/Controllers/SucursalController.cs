﻿using CQRS_MediatR.Features.TransactionFeatures.Commands;
using CQRS_MediatR.Features.TransactionFeatures.Queries;
using MediatR;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.Extensions.DependencyInjection;
namespace CQRS_MediatR.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class SucursalController : ControllerBase
    {
        private IMediator _mediator;
        protected IMediator Mediator => _mediator ??= HttpContext.RequestServices.GetService<IMediator>();
        
        [HttpGet]
        public async Task<IActionResult> GetAll(int pageNumer)
        {
            return Ok(await Mediator.Send(new GetAllSucursalQuery { PageNumer = pageNumer }));
        }
        //[HttpGet("{id}")]
        //public async Task<IActionResult> GetById(Guid id)
        //{
        //    return Ok(await Mediator.Send(new GetSucursalByIdQuery { Id = id }));
        //}
        [HttpGet("{nombre}")]
        public async Task<IActionResult> GetByNombre(string nombre)
        {
            return Ok(await Mediator.Send(new GetSucursalByNombreQuery { Nombre = nombre }));
        }
        [HttpPost]
        public async Task<IActionResult> Create(CreateSucursalCommand command)
        {
            return Ok(await Mediator.Send(command));
        }
        [HttpPut("{id}")]
        public async Task<IActionResult> Update(Guid id, UpdateSucursalCommand command)
        {
            if (id != command.Id)
            {
                return BadRequest();
            }
            return Ok(await Mediator.Send(command));
        }

        [HttpDelete("{id}")]
        public async Task<IActionResult> Delete(Guid id)
        {
            return Ok(await Mediator.Send(new DeleteSucursalByIdCommand { Id = id }));
        }
       
    }
}
